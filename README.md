# HSA-3 Homework project for Google Analytics (GAMP)

This an HSA-2 Homework project to show sending of custom offline metrics to Google Analytics.

Google Analytics page

![Google Analytics page](./screens/google-analytics-page.png?raw=true "Google Analytics page")

Real-time events

![Real-time events](./screens/real-time-events.png?raw=true "Real-time events")

USD/UAH event

![USD/UAH event](./screens/usd-uah-event.png?raw=true "USD/UAH event")

Ratio values first page

![Ratio values first page](./screens/ratio-values-first.png?raw=true "Ratio values first page")

Ratio values second page

![Ratio values second page](./screens/ratio-values-second.png?raw=true "Ratio values second page")

Engagement events

![Engagement events](./screens/google-analytics-engagement.png?raw=true "Engagement events")


## To reproduce results

Install requirements

```bash
$ pip install -r requirements.txt
```

Run sumilation

```bash
$ MEASUREMENT_ID=<measurment_id> API_SECRET=<api_secret> python main.py 
```

## License

The MIT License (MIT). Please see [License File](LICENSE) for more information.

